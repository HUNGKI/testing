﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyboxChanger : MonoBehaviour {

    public Material skybox;

	// Use this for initialization
	void Start () {
        RenderSettings.skybox = skybox;

	}
}
